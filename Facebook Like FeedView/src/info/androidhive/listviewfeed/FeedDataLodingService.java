package info.androidhive.listviewfeed;

import java.util.List;

import info.androidhive.listviewfeed.adapter.FeedListAdapter;
import info.androidhive.listviewfeed.data.FeedItem;

public interface FeedDataLodingService {
    public void loadData(String url, String subpath, FeedListAdapter adapter, List<FeedItem> feedItems, boolean isUsingCache);
	
}
