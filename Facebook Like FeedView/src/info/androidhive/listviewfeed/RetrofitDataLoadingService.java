package info.androidhive.listviewfeed;

import info.androidhive.listviewfeed.adapter.FeedListAdapter;
import info.androidhive.listviewfeed.data.FeedItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.util.Log;
import retrofit.RestAdapter;
import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.http.GET;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedOutput;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class RetrofitDataLoadingService implements FeedDataLodingService{
    
    private static final String TAG = "RetrofitDataLoadingService";
    
    public interface FeedApiInterface {
        @GET("/feed/feed.json")
        public Observable<String> getFeedData();
    }
    
    static class StringConverter implements Converter {

        @Override
        public Object fromBody(TypedInput typedInput, Type type) throws ConversionException {
            String text = null;
            try {
                text = fromStream(typedInput.in());
            } catch (IOException ignored) {/*NOP*/ }

            return text;
        }

        @Override
        public TypedOutput toBody(Object o) {
            return null;
        }

        public static String fromStream(InputStream in) throws IOException {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder out = new StringBuilder();
            String newLine = System.getProperty("line.separator");
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
                out.append(newLine);
            }
            return out.toString();
        }
    }

    @Override
    public void loadData(String url, String subpath, final FeedListAdapter adapter,
            final List<FeedItem> feedItems, boolean isUsingCache) {
        // TODO Auto-generated method stub
        RestAdapter restAdapter = new RestAdapter.Builder()
        .setEndpoint(url)
        .setConverter(new StringConverter())
        .setLogLevel(RestAdapter.LogLevel.FULL)
        .build();
        
        FeedApiInterface feedApiInterface = restAdapter.create(FeedApiInterface.class);
        feedApiInterface.getFeedData()
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<String>() {

                            @Override
                            public void onCompleted() {
                                // TODO Auto-generated method stub
                                
                            }

                            @Override
                            public void onError(Throwable arg0) {
                                // TODO Auto-generated method stub
                                
                            }

                            @Override
                            public void onNext(String result) {
                                // TODO Auto-generated method stub
                                JSONObject jObj;
                                try {
                                    jObj = new JSONObject(result);
                                    parseJsonFeed(jObj, adapter, feedItems);
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                        });
    }
    
    private void parseJsonFeed(JSONObject response, FeedListAdapter adapter, List<FeedItem> feedItems) {
        try {
            
            JSONArray feedArray = response.getJSONArray("feed");
            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                FeedItem item = new FeedItem();
                item.setId(feedObj.getInt("id"));
                item.setName(feedObj.getString("name"));

                // Image might be null sometimes
                String image = feedObj.isNull("image") ? null : feedObj
                        .getString("image");
                item.setImge(image);
                item.setStatus(feedObj.getString("status"));
                item.setProfilePic(feedObj.getString("profilePic"));
                item.setTimeStamp(feedObj.getString("timeStamp"));

                // url might be null sometimes
                String feedUrl = feedObj.isNull("url") ? null : feedObj
                        .getString("url");
                item.setUrl(feedUrl);

                feedItems.add(item);
            }

            // notify data changes to list adapater
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
	
}
