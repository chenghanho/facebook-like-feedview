package info.androidhive.listviewfeed;

import info.androidhive.listviewfeed.adapter.FeedListAdapter;
import info.androidhive.listviewfeed.app.AppController;
import info.androidhive.listviewfeed.data.FeedItem;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ListView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.android.volley.Cache;
import com.android.volley.Cache.Entry;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import de.greenrobot.event.EventBus;

public class MainActivity extends Activity {
	private static final String TAG = MainActivity.class.getSimpleName();
	
	private static final int LOADING_WITH_VOLLEY = 0;
	private static final int LOADING_WITH_RETROFIT = 1;
	
	@InjectView(R.id.list) ListView listView;
	private FeedListAdapter listAdapter;
	private List<FeedItem> feedItems;
	private String FEED_URL = "http://api.androidhive.info";
	private String FEED_SUBPATH = "/feed/feed.json";
	
	private FeedDataLodingService mService; 

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ButterKnife.inject(this);
		//listView = (ListView) findViewById(R.id.list);

		// These two lines not needed,
        // just to get the look of facebook (changing background color & hiding the icon)
        getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3b5998")));
        getActionBar().setIcon(
                   new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		
		feedItems = new ArrayList<FeedItem>();

		listAdapter = new FeedListAdapter(this, feedItems);
		listView.setAdapter(listAdapter);
		
		EventBus.getDefault().register(this); // register EventBus
		
		switch (LOADING_WITH_RETROFIT) {
		    case LOADING_WITH_RETROFIT :
                mService = new RetrofitDataLoadingService();
                break;
		    case LOADING_WITH_VOLLEY :
		    default:
		        mService = new VolleyDataLoadingService();
		        break;
		}
		mService.loadData(FEED_URL, FEED_SUBPATH, listAdapter, feedItems, true);
	}
	
	@Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this); // unregister EventBus
    }
	
	// method that will be called when someone posts an event NetworkStateChanged
    public void onEventMainThread(NetworkStateChanged event) {
        if (event.isInternetConnected()) {
            if(mService != null) {
                mService.loadData(FEED_URL, FEED_SUBPATH, listAdapter, feedItems, false);
            }
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
