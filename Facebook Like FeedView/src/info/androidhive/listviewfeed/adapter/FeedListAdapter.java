package info.androidhive.listviewfeed.adapter;

import info.androidhive.listviewfeed.FeedImageView;
import info.androidhive.listviewfeed.R;
import info.androidhive.listviewfeed.app.AppController;
import info.androidhive.listviewfeed.data.FeedItem;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class FeedListAdapter extends BaseAdapter {	
	private Activity activity;
	private LayoutInflater inflater;
	private List<FeedItem> feedItems;
	ImageLoader imageLoader = AppController.getInstance().getImageLoader();

	public FeedListAdapter(Activity activity, List<FeedItem> feedItems) {
		this.activity = activity;
		this.feedItems = feedItems;
	}
	
	static class ViewHolder {
	    @InjectView(R.id.name) TextView name;
        @InjectView(R.id.timestamp) TextView timestamp;
        @InjectView(R.id.txtStatusMsg) TextView statusMsg;
        @InjectView(R.id.txtUrl) TextView url;
        @InjectView(R.id.profilePic) NetworkImageView profilePic;
        @InjectView(R.id.feedImage1) FeedImageView feedImageView;
        
        public ViewHolder(View view){
            ButterKnife.inject(this, view);
        }
	}

	@Override
	public int getCount() {
		return feedItems.size();
	}

	@Override
	public Object getItem(int location) {
		return feedItems.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	    ViewHolder holder;

		if (inflater == null) {
		    inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		if (convertView == null) {
		    convertView = inflater.inflate(R.layout.feed_item, null);
		    holder = new ViewHolder(convertView);
		    convertView.setTag(holder);
		}
		else {
		    holder = (ViewHolder) convertView.getTag();
		}

		if (imageLoader == null) {
		    imageLoader = AppController.getInstance().getImageLoader();
		}
			

		FeedItem item = feedItems.get(position);

		holder.name.setText(item.getName());

		// Converting timestamp into x ago format
		CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
				Long.parseLong(item.getTimeStamp()),
				System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
		holder.timestamp.setText(timeAgo);

		// Chcek for empty status message
		if (!TextUtils.isEmpty(item.getStatus())) {
			holder.statusMsg.setText(item.getStatus());
			holder.statusMsg.setVisibility(View.VISIBLE);
		} else {
			// status is empty, remove from view
			holder.statusMsg.setVisibility(View.GONE);
		}

		// Checking for null feed url
		if (item.getUrl() != null) {
			holder.url.setText(Html.fromHtml("<a href=\"" + item.getUrl() + "\">"
					+ item.getUrl() + "</a> "));

			// Making url clickable
			holder.url.setMovementMethod(LinkMovementMethod.getInstance());
			holder.url.setVisibility(View.VISIBLE);
		} else {
			// url is null, remove from the view
			holder.url.setVisibility(View.GONE);
		}

		// user profile pic
		holder.profilePic.setImageUrl(item.getProfilePic(), imageLoader);
		

		// Feed image
		if (item.getImge() != null) {
			holder.feedImageView.setImageUrl(item.getImge(), imageLoader);
			holder.feedImageView.setVisibility(View.VISIBLE);
			holder.feedImageView
					.setResponseObserver(new FeedImageView.ResponseObserver() {
						@Override
						public void onError() {
						}

						@Override
						public void onSuccess() {
						}
					});
		} else {
			holder.feedImageView.setVisibility(View.GONE);
		}

		return convertView;
	}

}
