// Generated code from Butter Knife. Do not modify!
package info.androidhive.listviewfeed.adapter;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class FeedListAdapter$ViewHolder$$ViewInjector<T extends info.androidhive.listviewfeed.adapter.FeedListAdapter.ViewHolder> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296259, "field 'timestamp'");
    target.timestamp = finder.castView(view, 2131296259, "field 'timestamp'");
    view = finder.findRequiredView(source, 2131296257, "field 'profilePic'");
    target.profilePic = finder.castView(view, 2131296257, "field 'profilePic'");
    view = finder.findRequiredView(source, 2131296258, "field 'name'");
    target.name = finder.castView(view, 2131296258, "field 'name'");
    view = finder.findRequiredView(source, 2131296260, "field 'statusMsg'");
    target.statusMsg = finder.castView(view, 2131296260, "field 'statusMsg'");
    view = finder.findRequiredView(source, 2131296262, "field 'feedImageView'");
    target.feedImageView = finder.castView(view, 2131296262, "field 'feedImageView'");
    view = finder.findRequiredView(source, 2131296261, "field 'url'");
    target.url = finder.castView(view, 2131296261, "field 'url'");
  }

  @Override public void reset(T target) {
    target.timestamp = null;
    target.profilePic = null;
    target.name = null;
    target.statusMsg = null;
    target.feedImageView = null;
    target.url = null;
  }
}
