// Generated code from Butter Knife. Do not modify!
package info.androidhive.listviewfeed;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class MainActivity$$ViewInjector<T extends info.androidhive.listviewfeed.MainActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296256, "field 'listView'");
    target.listView = finder.castView(view, 2131296256, "field 'listView'");
  }

  @Override public void reset(T target) {
    target.listView = null;
  }
}
